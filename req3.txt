SQL> set autotrace on explain;
SQL> select AVG(DollarSales )
  2    from actvars a, ChanLevel ch , CustLevel c ,ProdLevel  p ,TimeLevel t
  3    where
  4    a.Customer_level=c.Store_level and
  5    a.Product_level = p.Code_level  and
  6    ch.Base_level IN('YJOOSEMVHWI6','LFXTUMHZ0TB5','BHOWG6O66A0A','JPGLNFAERH
HP
  7  ','JLEUMQV3GGPI') and
  8    a.Time_level  =  t.Tid      and
  9    ch.Base_level ='JPGLNFAERHHP' and
 10    t.year_level =1996 and
 11    p.Division_level
 12   IN('M0K2KU0C9XY4','XQ9TC5TK19BL','KLKDJXPZ2KW0');

AVG(DOLLARSALES)
----------------



Plan d'exÚcution
----------------------------------------------------------
Plan hash value: 3317180266

--------------------------------------------------------------------------------

-------

| Id  | Operation              | Name         | Rows  | Bytes | Cost (%CPU)| Tim

e     |

--------------------------------------------------------------------------------

-------

|   0 | SELECT STATEMENT       |              |     1 |    74 |     0   (0)|
      |

|   1 |  SORT AGGREGATE        |              |     1 |    74 |            |
      |

|*  2 |   FILTER               |              |       |       |            |
      |

|*  3 |    HASH JOIN           |              |    17M|  1234M| 58377   (2)| 00:

11:41 |

|*  4 |     TABLE ACCESS FULL  | PRODLEVEL    |  6750 |   171K|    33   (4)| 00:

00:01 |

|*  5 |     HASH JOIN          |              |    17M|   800M| 58221   (2)| 00:

11:39 |

|   6 |      NESTED LOOPS      |              |    12 |   288 |     3   (0)| 00:

00:01 |

|*  7 |       INDEX UNIQUE SCAN| SYS_C0011059 |     1 |    13 |     0   (0)| 00:

00:01 |

|*  8 |       TABLE ACCESS FULL| TIMELEVEL    |    12 |   132 |     3   (0)| 00:

00:01 |

|   9 |      TABLE ACCESS FULL | ACTVARS      |    24M|   567M| 58043   (1)| 00:

11:37 |

--------------------------------------------------------------------------------

-------


Predicate Information (identified by operation id):
---------------------------------------------------

   2 - filter(NULL IS NOT NULL)
   3 - access("A"."PRODUCT_LEVEL"="P"."CODE_LEVEL")
   4 - filter("P"."DIVISION_LEVEL"='KLKDJXPZ2KW0' OR
              "P"."DIVISION_LEVEL"='M0K2KU0C9XY4' OR "P"."DIVISION_LEVEL"='XQ9TC

5TK19BL')

   5 - access("A"."TIME_LEVEL"="T"."TID")
   7 - access("CH"."BASE_LEVEL"='JPGLNFAERHHP')
   8 - filter("T"."YEAR_LEVEL"=1996)
////////////////////////////////////////////////////////////
les indexes
 SQL> create bitmap index  bindexq4
  2  on actvars ( p.Line_level , t.year_level )
  3    from actvars a, ProdLevel  p ,TimeLevel t
  4      where
  5      a.Time_level  =  t.Tid and
  6       a.Product_level = p.Code_level
  7      tablespace indexes;

Index crÚÚ.

  SQL> create bitmap index  bindexq5
  2  on actvars ( ch.Base_level, c.Store_level  )
  3    from actvars a, ChanLevel ch , CustLevel c
  4      where
  5       a.Customer_level=c.Store_level and
  6      a.Channel_level = ch.Base_level
  7      tablespace indexes;

Index crÚÚ.




SQL> select /*+  index (a bindexq4,bindexq5)*/  AVG(DollarSales )
  2    from actvars a, ChanLevel ch , CustLevel c ,ProdLevel  p ,TimeLevel t
  3    where
  4    a.Customer_level=c.Store_level and
  5    a.Product_level = p.Code_level  and
  6    ch.Base_level IN('YJOOSEMVHWI6','LFXTUMHZ0TB5','BHOWG6O66A0A','JPGLNFAERH
HP
  7  ','JLEUMQV3GGPI') and
  8    a.Time_level  =  t.Tid      and
  9    ch.Base_level ='JPGLNFAERHHP' and
 10    t.year_level =1996 and
 11    p.Division_level
 12   IN('M0K2KU0C9XY4','XQ9TC5TK19BL','KLKDJXPZ2KW0');

AVG(DOLLARSALES)
----------------



Plan d'exÚcution
----------------------------------------------------------
Plan hash value: 4149126368

--------------------------------------------------------------------------------

---------

| Id  | Operation                | Name         | Rows  | Bytes | Cost (%CPU)| T

ime     |

--------------------------------------------------------------------------------

---------

|   0 | SELECT STATEMENT         |              |     1 |   100 |     0   (0)|
        |

|   1 |  SORT AGGREGATE          |              |     1 |   100 |            |
        |

|*  2 |   FILTER                 |              |       |       |            |
        |

|*  3 |    HASH JOIN             |              |  8748K|   834M| 58188   (2)| 0

0:11:39 |

|*  4 |     TABLE ACCESS FULL    | PRODLEVEL    |  6750 |   171K|    33   (4)| 0

0:00:01 |

|*  5 |     HASH JOIN            |              |  8748K|   617M| 58094   (2)| 0

0:11:38 |

|   6 |      INDEX FAST FULL SCAN| SYS_C0011062 |   900 | 11700 |     3   (0)| 0

0:00:01 |

|*  7 |      HASH JOIN           |              |  8748K|   508M| 58029   (1)| 0

0:11:37 |

|*  8 |       TABLE ACCESS FULL  | TIMELEVEL    |    12 |   132 |     3   (0)| 0

0:00:01 |

|   9 |       NESTED LOOPS       |              |    12M|   590M| 57938   (1)| 0

0:11:36 |

|* 10 |        INDEX UNIQUE SCAN | SYS_C0011059 |     1 |    13 |     0   (0)| 0

0:00:01 |

|  11 |        TABLE ACCESS FULL | ACTVARS      |    12M|   437M| 57938   (1)| 0

0:11:36 |

--------------------------------------------------------------------------------

---------


Predicate Information (identified by operation id):
---------------------------------------------------

   2 - filter(NULL IS NOT NULL)
   3 - access("A"."PRODUCT_LEVEL"="P"."CODE_LEVEL")
   4 - filter("P"."DIVISION_LEVEL"='KLKDJXPZ2KW0' OR
              "P"."DIVISION_LEVEL"='M0K2KU0C9XY4' OR "P"."DIVISION_LEVEL"='XQ9TC

5TK19BL')

   5 - access("A"."CUSTOMER_LEVEL"="C"."STORE_LEVEL")
   7 - access("A"."TIME_LEVEL"="T"."TID")
   8 - filter("T"."YEAR_LEVEL"=1996)
  10 - access("CH"."BASE_LEVEL"='JPGLNFAERHHP')

SQL>
